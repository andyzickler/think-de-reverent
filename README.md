# Intro

Decoder for archaic THINK Reference databases.
Maybe you used the THINK Reference Compiler tool back in the day, lost your original HTML and want your files back in a modern format?

# Disclaimer

I have no affiliation with THINK. This program is purely made through guesswork, is not guaranteed to work correctly and should not be used. Thanks.

# How would you get content?

One way would be to use an emulator like Basilisk II, load a .toast image you've made of your MacTech CDROM 1-17, whilst also having your 'unix' drive mapped and then drag the files to your host machine.

Previous I'd used DumpHFS to dump the content of an emulator .dsk image out to host machine and include .rdump files, these can be parsed using the macresources python library, but since its a text format its a lot slower, hence the switch to rsrc.

# Prerequisites

- Python 3
- deark - (utility to convert PICT resources to PNG, so good it doesn't even need headers) - https://github.com/jsummers/deark
- Imagemagick - to combine PNG when they come out as strips instead of whole images
  
# Python Libraries

- rsrcfork
- domonic
- curses

```pip3 install rsrcfork domonic curses```

# Usage

```python3 think-dr.py -i <databasefile>```

# Contributing

Please help!

[List of things to do](todo.md)
