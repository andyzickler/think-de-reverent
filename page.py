# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

from pkg_resources import parse_version
import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if parse_version(kaitaistruct.__version__) < parse_version('0.9'):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (
        kaitaistruct.__version__))


class Page(KaitaiStruct):
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.page_marker = self._io.read_bytes(2)
        if not self.page_marker == b"\x44\x52":
            raise kaitaistruct.ValidationNotEqualError(
                b"\x44\x52", self.page_marker, self._io, u"/seq/0")
        self.hyperlink_list_size = self._io.read_u2be()
        self.this_page_hyperink_index = self._io.read_u2be()
        self.hyperlink_list = [None] * (self.hyperlink_list_size)
        for i in range(self.hyperlink_list_size):
            self.hyperlink_list[i] = self._io.read_u2be()

        self.title = Page.PascalString(self._io, self, self._root)
        self.chunk_count = self._io.read_u2be()
        if self.chunk_count == 255:
            self.chunk_count2 = self._io.read_u2be()

        self.chunk = [
            None] * ((self.chunk_count2 if self.chunk_count == 255 else self.chunk_count))
        for i in range((self.chunk_count2 if self.chunk_count == 255 else self.chunk_count)):
            self.chunk[i] = Page.Chunk(self._io, self, self._root)

    class PascalString(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.length = self._io.read_u2be()
            self.text = (self._io.read_bytes(self.length)).decode(u"mac_roman")
            self._unnamed2 = self._io.read_bytes(((2 - self._io.pos()) % 2))

    class Chunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.unknown = self._io.read_u2be()
            self.ycoord = self._io.read_u2be()
            self.xcoord = self._io.read_u2be()
            self.content = Page.PascalString(self._io, self, self._root)
