meta:
  id: page
  endian: be
seq:
  - id: page_marker
    size: 2
  - id: hyperlink_list_size
    type: u2
  - id: this_page_hyperink_index
    type: u2
  - id: hyperlink_list
    type: u2
    repeat: expr
    repeat-expr: hyperlink_list_size
  - id: title
    type: pascal_string
  - id: chunk_count
    type: u2
  - id: chunk_count2
    type: u2
    if: chunk_count == 255
  - id: chunk
    type: chunk
    repeat: expr
    repeat-expr: chunk_count == 255 ? chunk_count2 : chunk_count
types:
  pascal_string:
    seq:
      - id: length
        type: u2
      - id: text
        type: str
        size: length
        # doesn't seem to support mac_roman?
        encoding: ascii 
        # add padding so the size of the page is a multiple of 2
        # https://github.com/kaitai-io/kaitai_struct/issues/12#issuecomment-277757932
      - size: (2 - _io.pos) % 2
  chunk:
    seq:
      - id: unknown
        type: u2
      - id: ycoord
        type: u2
      - id: xcoord
        type: u2
        # TODO : These are more complex and contain control codes
      - id: content
        type: pascal_string